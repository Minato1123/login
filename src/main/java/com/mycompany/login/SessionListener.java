/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/ServletListener.java to edit this template
 */
package com.mycompany.login;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Web application lifecycle listener.
 *
 * @author carrie
 */
public class SessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        // 監聽登入
        HttpSession session = se.getSession();
        ServletContext servletContext = session.getServletContext();
        servletContext.log("user logged in at " + session.toString());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        // 監聽登出
        HttpSession session = se.getSession();
        ServletContext servletContext = session.getServletContext();
        servletContext.log("user logged out");
    }
}
