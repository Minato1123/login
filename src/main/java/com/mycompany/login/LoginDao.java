/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.login;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carrie
 */
public class LoginDao {
    
    public void addLogin(Login login) {
        this.execute(new Task() {
            @Override
            public Object execute(Statement stmt) throws Exception {
                String id = login.getId();
                String password = login.getPassword();
                int ret = stmt.executeUpdate("INSERT INTO login (id, password) VALUES ('" + id + "', '" + password + "')");
                return null;
            }
        });
    }
    
    public void deleteLogin(Login login) {
        this.execute(new Task() {
            @Override
            public Object execute(Statement stmt) throws Exception {
                String id = login.getId();
                stmt.executeUpdate("DELETE FROM login WHERE id='" + id + "'");
                return null;
            }
        });
    }
    
    public List<Login> findAll() {
        List<Login> ret = new ArrayList<Login>();
        return (List<Login>) this.execute(new Task() {
            @Override
            public Object execute(Statement stmt) throws Exception {
                ResultSet rs = stmt.executeQuery("SELECT * FROM login");
                while (rs.next()) {
                    Login login = new Login();
                    login.setId(rs.getString("id"));
                    login.setPassword(rs.getString("password"));
                    ret.add(login);
                }
                return ret;

            };
        });
    }
    
    public Login findById(String id) {
        return (Login) this.execute(new Task() {
            @Override
            public Object execute(Statement stmt) throws Exception {
                ResultSet rs = stmt.executeQuery("SELECT * FROM login WHERE id='" + id + "'");
                if (rs.next()){
                    Login login = new Login();
                    login.setId(rs.getString("id"));
                    login.setPassword(rs.getString("password"));
                    return login;
                }
                return null;
            }
        });
    }
    
    public void updateLogin(Login login){
        this.execute(new Task() {
            @Override
            public Object execute(Statement stmt) throws Exception {
                String id = login.getId();
                String password = login.getPassword();
                int rs = stmt.executeUpdate("UPDATE login SET password='" + password + "' WHERE id='" + id + "'");
                return null;
            }
        });
    }
    
    public Object execute(Task task) {
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/mysql", "root", "")) {
            Statement stmt = conn.createStatement();
            return task.execute(stmt);
        } catch (Exception ex) {
            Logger.getLogger(LoginDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
