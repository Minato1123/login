/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.mycompany.login;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author carrie
 */
@WebServlet(name = "LoginListServlet", urlPatterns = {"/loginListServlet"})
public class LoginListServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        // 取得連線
        try ( PrintWriter out = response.getWriter()) {
            
            HttpSession session = request.getSession();
//            if (session.getAttribute("role").equals("user")) {
//                out.println("<style>");
//                out.println(".admin {display: none;}");
//                out.println("</style>");
//            }
            
            LoginDao loginDao = new LoginDao();
            List<Login> ret = loginDao.findAll();
//            out.println("<a href='addLogin.html'>ADD</a><br />");
            if (session.getAttribute("role").equals("admin")) {
                out.println("<form class='admin' method='post' action='addLogin'>");
                out.println("ID: <input type='text' name='id' /><br />");
                out.println("Password: <input type='password' name='password' /><br />");
                out.println("<input type='submit' value='Add' />");
                out.println("</form>");
            }
            
            for (Login login: ret) {
                out.println(login.getId());
                out.println(login.getPassword());
                if (session.getAttribute("role").equals("admin")) {
                    out.println("<a class='admin' href='deleteLogin?id=" + login.getId() + "'>Delete</a>");
                    out.println("<a class='admin' href='updateLogin.jsp?id=" + login.getId() + "'>Update</a>");
                }
                out.println("<br />");

            }
            out.println("<br /><a href='logout'>Logout</a>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
