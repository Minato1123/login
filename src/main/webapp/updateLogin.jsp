<%-- 
    Document   : updateLogin
    Created on : 2022年12月20日, 下午2:24:11
    Author     : carrie
--%>

<%@page import="com.mycompany.login.Login"%>
<%@page import="com.mycompany.login.LoginDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            LoginDao loginDao = new LoginDao();
            Login login = loginDao.findById(request.getParameter("id"));
        %>
        <form method="post" action="updateLogin">
            ID: <input style="margin-bottom: 1rem;" type="text" name="id" value="<%= login.getId() %>" /><br />
            Password: <input type="password" name="password" value="<%= login.getPassword() %>" /><br />
            
            <input type="submit" value="Update" />
        </form>
    </body>
</html>
